# Alania jeu RPG en console

Alania est un rpg console codé rust. Vous êtes de libre de le modifier. 

### TODO LIST

- [ ] Menu principal
- [ ] Nouvelle partie (création de personnage)
- [ ] Menu personnage
- [ ] Sauvegarder/Charger une partie
- [ ] Gestion des ennemis
- [ ] Système de combat tour par tour 
- [ ] Système de niveaux
- [ ] Système d'inventaire
- [ ] Système de boutique
- [ ] Evenements aléatoires, pas uniquement du combat
